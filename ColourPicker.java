package me.colour.picker;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ColourPicker extends JComponent {
	
	JFrame window;
	Container con;
	JPanel titleNamePanel, startButtonPanel, mainTextPanel, choiceButtonPanel, playerPanel;
	JLabel titleNameLabel, hpLabel, hpLabelNumber, weaponLabel, weaponLabelName;
	Font titleFont = new Font("Impact", Font.PLAIN, 90);
	Font normalFont = new Font("Impact", Font.PLAIN, 28);
	JButton startButton, choice1, choice2, choice3, choice4, choice5, choice6, choice7, choice8, choice9, choice10
	, choice11, choice12, choice13, choice14, choice15, choice16;
	JTextArea mainTextArea;
	int playerHP, monsterHP, silverRing;
	String weapon, position;
	
	TitleScreenHandler tsHandler = new TitleScreenHandler();
	ChoiceHandler choiceHandler = new ChoiceHandler();


	public static void main(String[] args) {

		new ColourPicker();
	}
	
	
	public ColourPicker(){
		
		window = new JFrame();
		window.setSize(1100, 600);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			URL url = new URL("https://i.imgur.com/xhYyZRi.jpg");
			Image image = ImageIO.read(url);
			window.setContentPane(new ImagePanel(image));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		window.setLayout(null);
		window.setVisible(true);
		window.setTitle("Bagroom colour picker");
		
		con = window.getContentPane();
		
		titleNamePanel = new JPanel();
		titleNamePanel.setBounds(250, 100, 600, 150);
		titleNamePanel.setBackground(new Color(0,0,0,0));
		titleNameLabel = new JLabel("Colour picker");
		titleNameLabel.setForeground(Color.white);
		titleNameLabel.setFont(titleFont);	
		
		choiceButtonPanel = new JPanel();
		choiceButtonPanel.setBounds(250, 350, 600, 150);
		choiceButtonPanel.setLayout(new GridLayout(4,1));
		con.add(choiceButtonPanel);
		
		Color white = Color.decode("#FFFFFF");
		Color red = Color.decode("#FF0000");
		Color green = Color.decode("#008000");
		Color yellow = Color.decode("#FFFF00");
		Color blue = Color.decode("#0000FF");
		Color navy = Color.decode("#000080");
		Color teal = Color.decode("#008080");
		Color maroon = Color.decode("#800000");
		Color purple = Color.decode("#800080");
		Color olive = Color.decode("#808000");
		Color silver = Color.decode("#C0C0C0");
		Color gray = Color.decode("#808080");
		Color lime = Color.decode("#00FF00");
		Color aqua = Color.decode("#00FFFF");
		Color fuchisa = Color.decode("#FF00FF");
		Color black = Color.decode("#000000");
		
		Color whitei = Color.decode("#000000");
		Color redi = Color.decode("#00FFFF");
		Color greeni = Color.decode("#FF7FFF");
		Color yellowi = Color.decode("#0000FF");
		Color bluei = Color.decode("#FFFF00");
		Color navyi = Color.decode("#FFFF7F");
		Color teali = Color.decode("#FF7F7F");
		Color marooni = Color.decode("#7FFFFF");
		Color purplei = Color.decode("#7FFF7F");
		Color olivei = Color.decode("#7F7FFF");
		Color silveri = Color.decode("#3F3F3F");
		Color grayi = Color.decode("#7f7f7f");
		Color limei = Color.decode("#FF00FF");
		Color aquai = Color.decode("#FF0000");
		Color fuchisai = Color.decode("#00FF00");
		Color blacki = Color.decode("#FFFFFF");
		
		choice1 = new JButton("Choice 1");
		choice1.setBackground(white);
		choice1.setForeground(whitei);
		choice1.setFont(normalFont);
		choice1.setFocusPainted(false);
		choice1.addActionListener(choiceHandler);
		choice1.setActionCommand("c1");
		choiceButtonPanel.add(choice1);
		
		choice2 = new JButton("Choice 2");
		choice2.setBackground(red);
		choice2.setForeground(redi);
		choice2.setFont(normalFont);
		choice2.setFocusPainted(false);
		choice2.addActionListener(choiceHandler);
		choice2.setActionCommand("c2");
		choiceButtonPanel.add(choice2);
		
		choice3 = new JButton("Choice 3");
		choice3.setBackground(green);
		choice3.setForeground(greeni);
		choice3.setFont(normalFont);
		choice3.setFocusPainted(false);
		choice3.addActionListener(choiceHandler);
		choice3.setActionCommand("c3");
		choiceButtonPanel.add(choice3);
		
		choice4 = new JButton("Choice 4");
		choice4.setBackground(yellow);
		choice4.setForeground(yellowi);
		choice4.setFont(normalFont);
		choice4.setFocusPainted(false);
		choice4.addActionListener(choiceHandler);
		choice4.setActionCommand("c4");
		choiceButtonPanel.add(choice4);
		
		choice5 = new JButton("Choice 5");
		choice5.setBackground(blue);
		choice5.setForeground(bluei);
		choice5.setFont(normalFont);
		choice5.setFocusPainted(false);
		choice5.addActionListener(choiceHandler);
		choice5.setActionCommand("c5");
		choiceButtonPanel.add(choice5);
		
		choice6 = new JButton("Choice 6");
		choice6.setBackground(navy);
		choice6.setForeground(navyi);
		choice6.setFont(normalFont);
		choice6.setFocusPainted(false);
		choice6.addActionListener(choiceHandler);
		choice6.setActionCommand("c6");
		choiceButtonPanel.add(choice6);
		
		choice7 = new JButton("Choice 7");
		choice7.setBackground(teal);
		choice7.setForeground(teali);
		choice7.setFont(normalFont);
		choice7.setFocusPainted(false);
		choice7.addActionListener(choiceHandler);
		choice7.setActionCommand("c7");
		choiceButtonPanel.add(choice7);
		
		choice8 = new JButton("Choice 8");
		choice8.setBackground(maroon);
		choice8.setForeground(marooni);
		choice8.setFont(normalFont);
		choice8.setFocusPainted(false);
		choice8.addActionListener(choiceHandler);
		choice8.setActionCommand("c8");
		choiceButtonPanel.add(choice8);
		
		choice9 = new JButton("Choice 9");
		choice9.setBackground(purple);
		choice9.setForeground(purplei);
		choice9.setFont(normalFont);
		choice9.setFocusPainted(false);
		choice9.addActionListener(choiceHandler);
		choice9.setActionCommand("c9");
		choiceButtonPanel.add(choice9);
		
		choice10 = new JButton("Choice 10");
		choice10.setBackground(olive);
		choice10.setForeground(olivei);
		choice10.setFont(normalFont);
		choice10.setFocusPainted(false);
		choice10.addActionListener(choiceHandler);
		choice10.setActionCommand("c10");
		choiceButtonPanel.add(choice10);
		
		choice11 = new JButton("Choice 11");
		choice11.setBackground(silver);
		choice11.setForeground(silveri);
		choice11.setFont(normalFont);
		choice11.setFocusPainted(false);
		choice11.addActionListener(choiceHandler);
		choice11.setActionCommand("c11");
		choiceButtonPanel.add(choice11);
		
		choice12 = new JButton("Choice 12");
		choice12.setBackground(gray);
		choice12.setForeground(Color.WHITE);
		choice12.setFont(normalFont);
		choice12.setFocusPainted(false);
		choice12.addActionListener(choiceHandler);
		choice12.setActionCommand("c12");
		choiceButtonPanel.add(choice12);
		
		choice13 = new JButton("Choice 13");
		choice13.setBackground(lime);
		choice13.setForeground(limei);
		choice13.setFont(normalFont);
		choice13.setFocusPainted(false);
		choice13.addActionListener(choiceHandler);
		choice13.setActionCommand("c13");
		choiceButtonPanel.add(choice13);
		
		choice14 = new JButton("Choice 14");
		choice14.setBackground(aqua);
		choice14.setForeground(aquai);
		choice14.setFont(normalFont);
		choice14.setFocusPainted(false);
		choice14.addActionListener(choiceHandler);
		choice14.setActionCommand("c14");
		choiceButtonPanel.add(choice14);
		
		choice15 = new JButton("Choice 15");
		choice15.setBackground(fuchisa);
		choice15.setForeground(fuchisai);
		choice15.setFont(normalFont);
		choice15.setFocusPainted(false);
		choice15.addActionListener(choiceHandler);
		choice15.setActionCommand("c15");
		choiceButtonPanel.add(choice15);
		
		choice16 = new JButton("Choice 16");
		choice16.setBackground(black);
		choice16.setForeground(blacki);
		choice16.setFont(normalFont);
		choice16.setFocusPainted(false);
		choice16.addActionListener(choiceHandler);
		choice16.setActionCommand("c16");
		choiceButtonPanel.add(choice16);
		
		position = "townGate";
		choice1.setText("White");
		choice2.setText("Red");
		choice3.setText("Green");
		choice4.setText("Yellow");
		choice5.setText("Blue");
		choice6.setText("Navy");
		choice7.setText("Teal");
		choice8.setText("Maroon");
		choice9.setText("Purple");
		choice10.setText("Olive");
		choice11.setText("Silver");
		choice12.setText("Gray");
		choice13.setText("Lime");
		choice14.setText("Aqua");
		choice15.setText("Fuchisa");
		choice16.setText("Black");
		
		titleNamePanel.add(titleNameLabel);
		
		con.add(titleNamePanel);
	}
	
	public void createGameScreen(){
		
		titleNamePanel.setVisible(false);
		startButtonPanel.setVisible(false);
		
		mainTextPanel = new JPanel();
		mainTextPanel.setBounds(100, 100, 600, 250);
		mainTextPanel.setBackground(Color.black);
		con.add(mainTextPanel);
		
		mainTextArea = new JTextArea("This is the main text are. This game is going to be great. I'm sure of it!!!!!!!");
		mainTextArea.setBounds(100, 100, 600, 250);
		mainTextArea.setBackground(Color.black);
		mainTextArea.setForeground(Color.white);
		mainTextArea.setFont(normalFont);
		mainTextArea.setLineWrap(true);
		mainTextPanel.add(mainTextArea);
		mainTextArea.setEditable(false);
		
		choiceButtonPanel = new JPanel();
		choiceButtonPanel.setBounds(250, 350, 800, 800);
		choiceButtonPanel.setBackground(Color.black);
		choiceButtonPanel.setLayout(new GridLayout(4,1));
		con.add(choiceButtonPanel);
		
		playerPanel = new JPanel();
		playerPanel.setBounds(100, 15, 600, 50);
		playerPanel.setBackground(Color.black);
		playerPanel.setLayout(new GridLayout(4,4));
		con.add(playerPanel);

	}
	public class TitleScreenHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
			
			createGameScreen();
		}
	}
	public class ChoiceHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
			
			String yourChoice = event.getActionCommand();
			
			switch(position){
			case "townGate":
				switch(yourChoice){
				case "c1": 
					String myString = "&%blockwhite%";
					StringSelection stringSelection = new StringSelection(myString);
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(stringSelection, null);
					break;
					
				case "c2":
					String myString2 = "&%blockred%";
					StringSelection stringSelection2 = new StringSelection(myString2);
					Clipboard clipboard2 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard2.setContents(stringSelection2, null);
				break;
				
				case "c3":
					String myString3 = "&%blockgreen%";
					StringSelection stringSelection3 = new StringSelection(myString3);
					Clipboard clipboard3 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard3.setContents(stringSelection3, null);
				break;
				
				case "c4":
					String myString4 = "&%blockyellow%";
					StringSelection stringSelection4 = new StringSelection(myString4);
					Clipboard clipboard4 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard4.setContents(stringSelection4, null);
				break;
				
				case "c5":
					String myString5 = "&%blockblue%";
					StringSelection stringSelection5 = new StringSelection(myString5);
					Clipboard clipboard5 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard5.setContents(stringSelection5, null);
				break;
				
				case "c6":
					String myString6 = "&%blocknavy%";
					StringSelection stringSelection6 = new StringSelection(myString6);
					Clipboard clipboard6 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard6.setContents(stringSelection6, null);
				break;
				
				case "c7":
					String myString7 = "&%blockteal%";
					StringSelection stringSelection7 = new StringSelection(myString7);
					Clipboard clipboard7 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard7.setContents(stringSelection7, null);
				break;
				
				case "c8":
					String myString8 = "&%blockmaroon%";
					StringSelection stringSelection8 = new StringSelection(myString8);
					Clipboard clipboard8 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard8.setContents(stringSelection8, null);
				break;
				
				case "c9":
					String myString9 = "&%blockpurple%";
					StringSelection stringSelection9 = new StringSelection(myString9);
					Clipboard clipboard9 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard9.setContents(stringSelection9, null);
				break;
				
				case "c10":
					String myString10 = "&%blockolive%";
					StringSelection stringSelection10 = new StringSelection(myString10);
					Clipboard clipboard10 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard10.setContents(stringSelection10, null);
				break;
				
				case "c11":
					String myString11 = "&%blocksilver%";
					StringSelection stringSelection11 = new StringSelection(myString11);
					Clipboard clipboard11 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard11.setContents(stringSelection11, null);
				break;
				
				case "c12":
					String myString12 = "&%blockgray%";
					StringSelection stringSelection12 = new StringSelection(myString12);
					Clipboard clipboard12 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard12.setContents(stringSelection12, null);
				break;
				
				case "c13":
					String myString13 = "&%blocklime%";
					StringSelection stringSelection13 = new StringSelection(myString13);
					Clipboard clipboard13 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard13.setContents(stringSelection13, null);
				break;
				
				case "c14":
					String myString14 = "&%blockaqua%";
					StringSelection stringSelection14 = new StringSelection(myString14);
					Clipboard clipboard14 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard14.setContents(stringSelection14, null);
				break;
				
				case "c15":
					String myString15 = "&%blockfuchisa%";
					StringSelection stringSelection15 = new StringSelection(myString15);
					Clipboard clipboard15 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard15.setContents(stringSelection15, null);
				break;
				
				case "c16":
					String myString16 = "&%blockblack%";
					StringSelection stringSelection16 = new StringSelection(myString16);
					Clipboard clipboard16 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard16.setContents(stringSelection16, null);
				break;
				
				}
			}
		}
	}
}
